﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Spice.Models {
    public class SubCategory {
        [Key]
        public int Id { get; set; }

        [Display (Name = "Tên danh mục")]
        [Required]
        public string Name { get; set; }

        [Required]
        [Display (Name = "Nhóm")]
        public int CategoryId { get; set; }

        [ForeignKey ("CategoryId")]
        public virtual Category Category { get; set; }

    }
}